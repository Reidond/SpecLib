#include "script_component.hpp"

#include "CfgEventHandlers.hpp"
#include "CfgMagazineWells.hpp"
#include "CfgMagazines.hpp"
#include "CfgPatches.hpp"
#include "CfgWeapons.hpp"
