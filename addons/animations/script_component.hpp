#define COMPONENT animations
#define COMPONENT_BEAUTIFIED Animations
#include "\z\speclib\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_TEST
#define DEBUG_MODE_FULL
#endif
#ifdef DEBUG_SETTINGS_TEST
#define DEBUG_SETTINGS DEBUG_SETTINGS_TEST
#endif

#include "\z\speclib\addons\main\script_macros.hpp"

#include "\a3\editor_f\Data\Scripts\dikCodes.h"

#define STATIC_ANIMATION(CLASSNAME)                                            \
    class CLASSNAME : CutSceneAnimationBase {                                  \
        file = \z\speclib\addons\animations_data\##CLASSNAME##.rtm;            \
        speed = 0.010877;                                                      \
        actions = "StaticCutsceneLoop";                                        \
        ConnectTo[] = {"AmovPercMstpSnonWnonDnon", 0.01};                      \
        InterpolateTo[] = {};                                                  \
    }

#define ACE_STATIC_ANIMATION(CLASSNAME, DISPLAYNAME, ANIMATION)                \
    class spec_##CLASSNAME {                                                   \
        displayName = DISPLAYNAME;                                             \
        condition = "";                                                        \
        exceptions[] = {};                                                     \
        statement = QUOTE(_player switchMove ANIMATION);                       \
        icon = "\z\speclib\addons\animations\icons\ui_dot.paa";                \
    }

#define ACE_STATIC_REMOTE_ANIMATION(CLASSNAME, DISPLAYNAME, ANIMATION)              \
    class spec_##CLASSNAME {                                                        \
        displayName = DISPLAYNAME;                                                  \
        condition = "";                                                             \
        exceptions[] = {};                                                          \
        statement = [ace_player, ANIMATION] remoteExec ['switchMove', 0];           \
        icon = "\z\speclib\addons\animations\icons\ui_dot.paa";                     \
    }
