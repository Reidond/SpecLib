class CfgMovesBasic {
    class ManActions;
    class Actions {
        class NoActions : ManActions {};
        class StaticCutsceneLoop : NoActions {
            upDegree = "ManPosStand";
            stance = "ManStanceStand";
            LadderOnDown = "";
            LadderOnUp = "";
            TestDriver = "";
            TestDriverOut = "";
            TestGunner = "";
            Die = "DeadState";
        };
    };
};
