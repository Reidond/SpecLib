#include "script_component.hpp"
class CfgPatches {
    class ADDON {
        name = COMPONENT;
        units[] = {
            "SpecLib_O_R_Assault_TL_01_F",
            "SpecLib_O_R_Assault_TL_02_F",
            "SpecLib_O_R_Assault_MachineGunner_F",
            "SpecLib_O_R_Assault_medic_01_F",
            "SpecLib_O_R_Assault_medic_02_F",
            "SpecLib_O_R_Assault_M_01_F",
            "SpecLib_O_R_Assault_M_02_F",
            "SpecLib_O_R_Assault_01_F",
            "SpecLib_O_R_Assault_02_F",
            "SpecLib_O_R_Assault_GL_01_F",
            "SpecLib_O_R_Assault_GL_02_F",
            "SpecLib_O_R_Assault_AT_01_F",
            "SpecLib_O_R_Assault_AT_02_F",
            "SpecLib_O_R_Assault_AT_03_F",
            "SpecLib_O_R_Assault_AT_04_F"
        };
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"cba_main", "ace_main"};
        author = "Reidond";
        VERSION_CONFIG;
    };
};

class CBA_Extended_EventHandlers;

#include "CfgEditorSubcategories.hpp"
#include "CfgEventHandlers.hpp"
#include "CfgVehicles.hpp"
