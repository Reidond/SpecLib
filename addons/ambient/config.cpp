#include "script_component.hpp"
class CfgPatches {
    class ADDON {
        name = COMPONENT;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"cba_main", "ace_main", "A3_Ui_F"};
        author = "Reidond";
        VERSION_CONFIG;
    };
};

class RscDisplayLoadmission;
class RscText;
class RscPicture;
class RscVignette;
class RscPictureKeepAspect;
class RscDisplayMain : RscStandardDisplay {
    enableDisplay = 0;
    class Spotlight {};
    class controls {
        class Spotlight1 {};
        class Spotlight2 {};
        class Spotlight3 {};
        class BackgroundSpotlightRight {};
        class BackgroundSpotlightLeft {};
        class BackgroundSpotlight {};
    };
};

#include "CfgEventHandlers.hpp"
#include "CfgMusicClasses.hpp"
#include "CfgMusic.hpp"
#include "CfgMissions.hpp"
#include "CfgWorlds.hpp"
