private _musicPool = "getText (_x >> 'musicClass') == 'IntroAmbient'" configClasses (configFile >> "CfgMusic");

private _randomFirstTrack = _musicPool call BIS_fnc_selectRandom;

[_randomFirstTrack] call CBA_fnc_playMusic;

addMusicEventHandler ["MusicStop", {
    _randomTrack = _musicPool call BIS_fnc_selectRandom;
    0 fadeMusic 0;
    [_randomTrack] call CBA_fnc_playMusic;
    10 fadeMusic 1;
}];
