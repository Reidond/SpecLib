class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class rhs_western_rifle_scopes_slot_short;
class rhs_western_rifle_laser_slot;
class rhs_western_rifle_muzzle_slot;
class rhs_western_rifle_gripod_slot;
class CfgWeapons {
    class Rifle;
    class Rifle_Base_F : Rifle {
        class WeaponSlotsInfo;
        class GunParticles;
        class Eventhandlers;
    };

    class gs_vss : Rifle_Base_F {
        class Single : Mode_SemiAuto {
            class BaseSoundModeType {
                weaponSoundEffect = "DefaultRifle";
            };
            class StandardSound : BaseSoundModeType {
                soundSetShot[] = {"RHS_ASVAL_Closure_SoundSet",
                                  "RHS_ASVAL_ShotSD_SoundSet",
                                  "RHS_rifle1_SD_Tail_SoundSet"};
            };
            class SilencedSound : BaseSoundModeType {};
            aiDispersionCoefX = 1.1;
            aiDispersionCoefY = 1.2;
            reloadTime = 0.1;
            dispersion = 0.00063;
            recoil = "recoil_single_primary_3outof10";
            recoilProne = "recoil_single_primary_prone_3outof10";
            minRange = 2;
            minRangeProbab = 0.3;
            midRange = 100;
            midRangeProbab = 0.7;
            maxRange = 420;
            maxRangeProbab = 0.45;
            aiRateOfFire = 3;
            aiRateOfFireDistance = 400;
        };
        class FullAuto : Mode_FullAuto {
            class BaseSoundModeType {
                weaponSoundEffect = "DefaultRifle";
            };
            class StandardSound : BaseSoundModeType {
                soundSetShot[] = {"RHS_ASVAL_Closure_SoundSet",
                                  "RHS_ASVAL_ShotSD_SoundSet",
                                  "RHS_rifle1_SD_Tail_SoundSet"};
            };
            class SilencedSound : BaseSoundModeType {};
            aiDispersionCoefX = 1.2;
            aiDispersionCoefY = 1.4;
            reloadTime = 0.1;
            dispersion = 0.00063;
            recoil = "recoil_auto_primary_3outof10";
            recoilProne = "recoil_auto_primary_prone_3outof10";
            minRange = 0;
            minRangeProbab = 0.9;
            midRange = 20;
            midRangeProbab = 0.7;
            maxRange = 35;
            maxRangeProbab = 0.1;
        };
    };
    class gs_asval : gs_vss {
        L_ES_Prefix = "nosound";
        L_ES_maxPlopp = 0;
        class Single : Mode_SemiAuto {
            class BaseSoundModeType {
                weaponSoundEffect = "DefaultRifle";
            };
            class StandardSound : BaseSoundModeType {
                soundSetShot[] = {"RHS_ASVAL_Closure_SoundSet",
                                  "RHS_ASVAL_ShotSD_SoundSet",
                                  "RHS_rifle1_SD_Tail_SoundSet"};
            };
            class SilencedSound : BaseSoundModeType {};
            reloadTime = 0.0667;
            dispersion = 0.00083;
            aiDispersionCoefX = 1.2;
            aiDispersionCoefY = 1.4;
            recoil = "recoil_single_primary_3outof10";
            recoilProne = "recoil_single_primary_prone_3outof10";
            minRange = 2;
            minRangeProbab = 0.3;
            midRange = 100;
            midRangeProbab = 0.7;
            maxRange = 420;
            maxRangeProbab = 0.45;
            aiRateOfFire = 3;
            aiRateOfFireDistance = 400;
        };
        class FullAuto : Mode_FullAuto {
            class BaseSoundModeType {
                weaponSoundEffect = "DefaultRifle";
            };
            class StandardSound : BaseSoundModeType {
                soundSetShot[] = {"RHS_ASVAL_Closure_SoundSet",
                                  "RHS_ASVAL_ShotSD_SoundSet",
                                  "RHS_rifle1_SD_Tail_SoundSet"};
            };
            class SilencedSound : BaseSoundModeType {};
            reloadTime = 0.0667;
            dispersion = 0.00083;
            aiDispersionCoefX = 1.3;
            aiDispersionCoefY = 1.5;
            recoil = "recoil_auto_primary_3outof10";
            recoilProne = "recoil_auto_primary_prone_3outof10";
            minRange = 0;
            minRangeProbab = 0.9;
            midRange = 20;
            midRangeProbab = 0.7;
            maxRange = 35;
            maxRangeProbab = 0.1;
        };
    };
    class gs_sr3mp : gs_asval {
        class Single : Mode_SemiAuto {
            class BaseSoundModeType {
                weaponSoundEffect = "DefaultRifle";
            };
            class StandardSound : BaseSoundModeType {
                SoundSetShot[] = {"GS_sr3m_Shot_SoundSet",
                                  "SMGPDW2000_Tail_SoundSet",
                                  "SMGPDW2000_InteriorTail_SoundSet"};
            };
            class SilencedSound : BaseSoundModeType {
                soundSetShot[] = {"RHS_ak74_Closure_SoundSet",
                                  "RHS_ak74_ShotSD_SoundSet",
                                  "RHS_Rifle1_SD_Tail_SoundSet"};
            };
            reloadTime = 0.066699997;
            dispersion = 0.00153;
            recoil = "recoil_single_primary_3outof10";
            recoilProne = "recoil_single_primary_prone_3outof10";
            minRange = 2;
            minRangeProbab = 0.30000001;
            midRange = 100;
            midRangeProbab = 0.69999999;
            maxRange = 200;
            maxRangeProbab = 0.050000001;
            aiRateOfFire = 3;
            aiRateOfFireDistance = 200;
        };
        class FullAuto : Mode_FullAuto {
            class BaseSoundModeType {
                weaponSoundEffect = "DefaultRifle";
            };
            class StandardSound : BaseSoundModeType {
                SoundSetShot[] = {"GS_sr3m_Shot_SoundSet",
                                  "SMGPDW2000_Tail_SoundSet",
                                  "SMGPDW2000_InteriorTail_SoundSet"};
            };
            class SilencedSound : BaseSoundModeType {
                soundSetShot[] = {"RHS_ak74_Closure_SoundSet",
                                  "RHS_ak74_ShotSD_SoundSet",
                                  "RHS_Rifle1_SD_Tail_SoundSet"};
            };
            reloadTime = 0.066699997;
            dispersion = 0.00153;
            recoil = "recoil_auto_primary_3outof10";
            recoilProne = "recoil_auto_primary_prone_3outof10";
            minRange = 0;
            minRangeProbab = 0.69999999;
            midRange = 15;
            midRangeProbab = 0.89999998;
            maxRange = 25;
            maxRangeProbab = 0.1;
        };
    };
};
