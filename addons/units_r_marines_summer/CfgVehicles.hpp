class CfgVehicles {
    class YuE_6sh92rFl26b23;
    class rhs_rpg_empty;
    class rhs_medic_bag;
    class O_R_Soldier_base_F;
    class O_R_Soldier_TL_F;
    class O_R_Soldier_AR_F;
    class O_R_Soldier_LAT_F;
    class O_R_medic_F;
    class O_R_soldier_M_F;


    class YuE_6sh92rFl26b23_Marine_01_F : YuE_6sh92rFl26b23 {
        scope = 1;
        class TransportMagazines {
            mag_xx(rhs_100Rnd_762x54mmR_7N13, 2);
        };
        class TransportItems {
            item_xx(VTN_1PN74, 1);
        };
    };

    class YuE_6sh92rFl26b23_Marine_02_F : YuE_6sh92rFl26b23 {
        scope = 1;
        class TransportMagazines {
            mag_xx(SPEC_45Rnd_545x39_7N10_RPK74M, 8);
        };
        class TransportItems {
            item_xx(VTN_1PN74, 1);
        };
    };

    class YuE_6sh92rFl26b23_Marine_03_F : YuE_6sh92rFl26b23 {
        scope = 1;
        class TransportItems {
            item_xx(VTN_1PN74, 1);
            item_xx(ToolKit, 1);
            item_xx(ACE_EntrenchingTool, 1);
        };
    };

    class YuE_6sh92rFl26b23_Marine_04_F : YuE_6sh92rFl26b23 {
        scope = 1;
        class TransportMagazines {
            mag_xx(rhs_mag_rdg2_white, 4);
        };
        class TransportItems {
            item_xx(ACE_fieldDressing, 14);
            item_xx(ACE_elasticBandage, 14);
            item_xx(ACE_quikclot, 14);
            item_xx(ACE_packingBandage, 14);
            item_xx(ACE_tourniquet, 4);
            item_xx(ACE_morphine, 6);
            item_xx(ACE_epinephrine, 6);
            item_xx(ACE_adenosine, 6);
            item_xx(ACE_salineIV, 2);
            item_xx(ACE_salineIV_500, 4);
            item_xx(ACE_surgicalKit, 1);
            item_xx(ACE_splint, 6);
        };
    };

    class YuE_6sh92rFl26b23_Marine_05_F : YuE_6sh92rFl26b23 {
        scope = 1;
        class TransportMagazines {
            mag_xx(rhs_ec200_mag, 2);
            mag_xx(rhs_mine_ozm72_a_mag, 2);  
        };
        class TransportItems {
            item_xx(VTN_1PN74, 1);
            item_xx(MineDetector, 1);
            item_xx(ACE_DefusalKit, 1);
            item_xx(ACE_Clacker, 1);
        };
    };

    class rhs_rpg_empty_marine_01_F : rhs_rpg_empty {
        scope = 1;
        class TransportMagazines {
            mag_xx(rhs_rpg7_PG7VL_mag, 3);
        };
    };

    class SpecLib_O_R_S_Marine_exp_F : O_R_Soldier_base_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_engineer_F);

		canDeactivateMines = 1;
        icon = "iconManExplosive";
        role = "Sapper";

        uniformClass = "Peh_Flora_EAST_Uniform2";

        backpack = "YuE_6sh92rFl26b23_Marine_05_F";

        displayName = "Сапёр";
        weapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_crew_TL_F : O_R_Soldier_TL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_crew_TL_F);

        uniformClass = "Peh_Flora_EAST_Uniform2";

        engineer = 1;

        displayName = "Командир экипажа";
        weapons[] = {"CUP_arifle_AKS74U", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKS74U", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_5("rhs_30Rnd_545x39_7N10_AK"), GRENADES};
        respawnMagazines[] = {mag_5("rhs_30Rnd_545x39_7N10_AK"), GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"rhs_6sh46", "rhs_tsh4", LINKED_ITEMS_CREW};
        respawnLinkedItems[] = {"rhs_6sh46", "rhs_tsh4", LINKED_ITEMS_CREW};
    };
    class SpecLib_O_R_S_Marine_crew_F : O_R_Soldier_base_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_crew_F);

        uniformClass = "Peh_Flora_EAST_Uniform2";

        engineer = 1;

        displayName = "Член экипажа";
        weapons[] = {"CUP_arifle_AKS74U", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKS74U", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_5("rhs_30Rnd_545x39_7N10_AK"), GRENADES};
        respawnMagazines[] = {mag_5("rhs_30Rnd_545x39_7N10_AK"), GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"rhs_6sh46", "rhs_tsh4", LINKED_ITEMS_CREW};
        respawnLinkedItems[] = {"rhs_6sh46", "rhs_tsh4", LINKED_ITEMS_CREW};
    };
    class SpecLib_O_R_S_Marine_Officer_F : O_R_Soldier_TL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_Officer_F);

        uniformClass = "Peh_Flora_EAST_Uniform2";

        role = "Rifleman";
        icon = "iconManOfficer";

        displayName = "Офицер";
        weapons[] = {"CUP_arifle_AK74M",
                     "VTN_PYA",
                     "VTN_B8",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M",
                            "VTN_PYA",
                            "VTN_B8",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       mag_3("VTN_PYA_18s_PS"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              mag_3("VTN_PYA_18s_PS"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2",
                         "rhs_beret_mp2",
                         LINKED_ITEMS_OFFICER};
        respawnLinkedItems[] = {"6b23_6sh92Fl2",
                                "rhs_beret_mp2",
                                LINKED_ITEMS_OFFICER};
    };
    class SpecLib_O_R_S_Marine_TL_F : O_R_Soldier_TL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_TL_F);

        uniformClass = "Peh_Flora_EAST_Uniform2";

        displayName = "Командир отделения";
        weapons[] = {"CUP_arifle_AK74M_GL_1p63",
                     "VTN_PYA",
                     "VTN_B8",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_GL_1p63",
                            "VTN_PYA",
                            "VTN_B8",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       mag_3("VTN_PYA_18s_PS"),
                       GRENADE_LAUNCHER_ROUNDS,
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              mag_3("VTN_PYA_18s_PS"),
                              GRENADE_LAUNCHER_ROUNDS,
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"6b23_6sh92gpFl2",
                         "rhs_6b26",
                         "YuEGBSHP2",
                         LINKED_ITEMS_TL};
        respawnLinkedItems[] = {"6b23_6sh92gpFl2",
                                "rhs_6b26",
                                "YuEGBSHP2",
                                LINKED_ITEMS_TL};                       
    };
    class SpecLib_O_R_S_Marine_MG_F : O_R_Soldier_AR_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_MG_F);

        uniformClass = "Peh_Flora_EAST_Uniform2";

        backpack = "YuE_6sh92rFl26b23_Marine_01_F";

        role = "MachineGunner";
        icon = "iconManMG";

        displayName = "Пулемётчик";
        weapons[] = {"VTN_PKMN", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"VTN_PKMN", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_3("rhs_100Rnd_762x54mmR_7N13"),
                       GRENADES};
        respawnMagazines[] = {mag_3("rhs_100Rnd_762x54mmR_7N13"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"6b23_SPK_FO", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_SPK_FO", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_AMG_F : SpecLib_O_R_S_Marine_MG_F {
        role = "Assistant";
        icon = "iconMan";
        PREVIEW(SpecLib_O_R_S_Marine_AMG_F);
        displayName = "Помощник пулемётчика";
        weapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),   
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_AR_F : O_R_Soldier_AR_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_AR_F);
        uniformClass = "Peh_Flora_EAST_Uniform2";

        backpack = "YuE_6sh92rFl26b23_Marine_02_F";

        role = "MachineGunner";
        icon = "iconManMG";

        displayName = "Пулемётчик (РПК-74М)";
        weapons[] = {"CUP_arifle_RPK74M", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_RPK74M", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("SPEC_45Rnd_545x39_7N10_RPK74M"), GRENADES};
        respawnMagazines[] = {mag_9("SPEC_45Rnd_545x39_7N10_RPK74M"), GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_AT_F : O_R_Soldier_LAT_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_AT_F);

        uniformClass = "Peh_Flora_EAST_Uniform2";

        backpack = "rhs_rpg_empty_marine_01_F";

        displayName = "Гранатомётчик";
        weapons[] = {"CUP_arifle_AK74M", "rhs_weap_rpg7_pgo7v3", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M",
                            "rhs_weap_rpg7_pgo7v3",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       "rhs_rpg7_PG7VL_mag",
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),  
                              "rhs_rpg7_PG7VL_mag",
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_AAT_F : SpecLib_O_R_S_Marine_AT_F {
        role = "Assistant";
        icon = "iconMan";
        PREVIEW(SpecLib_O_R_S_Marine_AAT_F);
        displayName = "Помощник гранатомётчика";
        weapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
    };
    class SpecLib_O_R_S_Marine_medic_F : O_R_medic_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_medic_F);

        uniformClass = "Peh_Flora_EAST_Uniform2";

        backpack = "YuE_6sh92rFl26b23_Marine_04_F";

        displayName = "Санитар";
        weapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       GRENADES_MEDIC};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              GRENADES_MEDIC};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_engineer_F : O_R_Soldier_base_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_engineer_F);

        engineer = 1;
        icon = "iconManEngineer";
        role = "Sapper";

        uniformClass = "Peh_Flora_EAST_Uniform2";

        backpack = "YuE_6sh92rFl26b23_Marine_03_F";

        displayName = "Инженер";
        weapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_F : O_R_Soldier_base_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_F);

        uniformClass = "Peh_Flora_EAST_Uniform2";

        displayName = "Стрелок";
        weapons[] = {"CUP_arifle_AK74M_1p29", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_1p29", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_GL_F : SpecLib_O_R_S_Marine_F {
        PREVIEW(SpecLib_O_R_S_Marine_GL_F);
        displayName = "Стрелок ГП";
        weapons[] = {"CUP_arifle_AK74M_GL", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_GL",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       GRENADE_LAUNCHER_ROUNDS,
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              GRENADE_LAUNCHER_ROUNDS,
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"6b23_6sh92gpFl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92gpFl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_LAT_01_F : O_R_Soldier_base_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_LAT_01_F);

        icon = "iconManAT";
        role = "MissileSpecialist";

        uniformClass = "Peh_Flora_EAST_Uniform2";

        displayName = "Стрелок (РПГ-26)";
        weapons[] = {"CUP_arifle_AK74M_1p29",
                     "rhs_weap_rpg26",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_1p29",
                            "rhs_weap_rpg26",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       "rhs_rpg26_mag",
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              "rhs_rpg26_mag",
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_LAT_02_F : SpecLib_O_R_S_Marine_LAT_01_F {
        PREVIEW(SpecLib_O_R_S_Marine_LAT_02_F);

        icon = "iconManAT";
        role = "MissileSpecialist";

        displayName = "Стрелок (РШГ-2)";
        weapons[] = {"CUP_arifle_AK74M",
                     "rhs_weap_rshg2",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M",
                            "rhs_weap_rshg2",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       "rhs_rshg2_mag",
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              "rhs_rshg2_mag",
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
    };
    class SpecLib_O_R_S_Marine_AA_F : O_R_Soldier_base_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_AA_F);

        icon = "iconManAT";
        role = "MissileSpecialist";

        uniformClass = "Peh_Flora_EAST_Uniform2";

        displayName = "Оператор ПЗРК";
        weapons[] = {"CUP_arifle_AK74M", "rhs_weap_igla", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M",
                            "rhs_weap_igla",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                       "rhs_mag_9k38_rocket",
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N10_AK"),
                              "rhs_mag_9k38_rocket",
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92Fl2", "rhs_6b26", LINKED_ITEMS};
    };
    class SpecLib_O_R_S_Marine_M_F : O_R_soldier_M_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_S_Marine_M_F);

        uniformClass = "Peh_Flora_EAST_Uniform2";

        displayName = "Снайпер";
        weapons[] = {"VTN_SVD_PSO1M2", "VTN_PYA", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"VTN_SVD_PSO1M2", "VTN_PYA", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_13("VTN_SVD_10s_SC"),
                       mag_3("VTN_PYA_18s_PS"),
                       GRENADES};
        respawnMagazines[] = {mag_13("VTN_SVD_10s_SC"),
                              mag_3("VTN_PYA_18s_PS"),
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"6b23_6sh92svdFl2", "rhs_6b26", LINKED_ITEMS};
        respawnLinkedItems[] = {"6b23_6sh92svdFl2", "rhs_6b26", LINKED_ITEMS};
    };
};
