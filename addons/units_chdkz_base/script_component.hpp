#define COMPONENT units_chdkz_base
#include "\z\speclib\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_UNITS_CHDKZ_BASE
    #define DEBUG_MODE_FULL
#endif
    #ifdef DEBUG_SETTINGS_UNITS_CHDKZ_BASE
    #define DEBUG_SETTINGS DEBUG_SETTINGS_UNITS_CHDKZ_BASE
#endif

#include "\z\speclib\addons\main\script_macros.hpp"
