#include "script_component.hpp"
class CfgPatches {
    class ADDON {
        name = COMPONENT;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {
            "cba_main",
            "ace_main",
            "rhs_c_troops",
            "rhsgref_main",
        };
        author = "Reidond";
        VERSION_CONFIG;
    };
};

#include "CfgEventHandlers.hpp"

class DefaultEventhandlers;
class CfgVehicles {
    class Land;
    class Man : Land {
        class EventHandlers;
    };
    class CAManBase : Man {
        class HitPoints;
    };
    class SoldierEB;
    class SoldierGB : CAManBase {
        class HitPoints : HitPoints {
            class HitFace;
            class HitNeck;
            class HitHead;
            class HitPelvis;
            class HitAbdomen;
            class HitDiaphragm;
            class HitChest;
            class HitBody;
            class HitArms;
            class HitHands;
            class HitLegs;
        };
        class Eventhandlers;
    };
    class rhs_infantry_msv_base : SoldierGB {
        class EventHandlers;
    };
    class rhs_msv_rifleman : rhs_infantry_msv_base {};
    class rhs_msv_emr_rifleman : rhs_msv_rifleman {};
    class T_CHDKZ_chedak1 : rhs_msv_emr_rifleman {
        scope = 1;
        scopeCurator = 0;
        identityTypes[] = {"LanguageRUS", "Head_Russian", "Head_Euro"};
        uniformClass = "T_CHDKZ_chedak1_uniform";
        model = "\rhsafrf\addons\rhs_infantry2\rhs_emr_base.p3d";
        selectionClan = "";
        class EventHandlers : EventHandlers {
            init = "";
        };
        headgearList[] = {};
        hiddenSelections[] = {"camo1", "camo2", "camob", "insignia", "clan"};
        hiddenSelectionsTextures[] = {
            "z\speclib\addons\units_chdkz_base\textures\officer_co.paa",
            "rhsafrf\addons\rhs_infantry\data\shevrons_vdv_co.paa",
            "rhsafrf\addons\rhs_infantry\data\digi_chevrons_co.paa",
            "",
            ""};
        class Wounds {
            tex[] = {};
            mat[] = {
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w1.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w2.rvmat",
                "A3\Characters_F\Common\Data\basicbody.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\characters_f\common\data\coveralls.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"};
        };
    };
    class T_CHDKZ_chedak2 : rhs_msv_emr_rifleman {
        scope = 1;
        scopeCurator = 0;
        identityTypes[] = {"LanguageRUS", "Head_Russian", "Head_Euro"};
        uniformClass = "T_CHDKZ_chedak2_uniform";
        model = "\rhsafrf\addons\rhs_infantry2\rhs_emr_base.p3d";
        selectionClan = "";
        class EventHandlers : EventHandlers {
            init = "";
        };
        headgearList[] = {};
        hiddenSelections[] = {"camo1", "camo2", "camob", "insignia", "clan"};
        hiddenSelectionsTextures[] = {
            "z\speclib\addons\units_chdkz_base\textures\commander_co.paa",
            "rhsafrf\addons\rhs_infantry\data\shevrons_vdv_co.paa",
            "rhsafrf\addons\rhs_infantry\data\digi_chevrons_co.paa",
            "",
            ""};
        class Wounds {
            tex[] = {};
            mat[] = {
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w1.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w2.rvmat",
                "A3\Characters_F\Common\Data\basicbody.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\characters_f\common\data\coveralls.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"};
        };
    };
    class T_CHDKZ_chedak3 : rhs_msv_emr_rifleman {
        scope = 1;
        scopeCurator = 0;
        identityTypes[] = {"LanguageRUS", "Head_Russian", "Head_Euro"};
        uniformClass = "T_CHDKZ_chedak3_uniform";
        model = "\rhsafrf\addons\rhs_infantry2\rhs_emr_base.p3d";
        selectionClan = "";
        class EventHandlers : EventHandlers {
            init = "";
        };
        headgearList[] = {};
        hiddenSelections[] = {"camo1", "camo2", "camob", "insignia", "clan"};
        hiddenSelectionsTextures[] = {
            "z\speclib\addons\units_chdkz_base\textures\soldier1_co.paa",
            "rhsafrf\addons\rhs_infantry\data\shevrons_vdv_co.paa",
            "rhsafrf\addons\rhs_infantry\data\digi_chevrons_co.paa",
            "",
            ""};
        class Wounds {
            tex[] = {};
            mat[] = {
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w1.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w2.rvmat",
                "A3\Characters_F\Common\Data\basicbody.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\characters_f\common\data\coveralls.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"};
        };
    };
    class T_CHDKZ_chedak4 : rhs_msv_emr_rifleman {
        scope = 1;
        scopeCurator = 0;
        identityTypes[] = {"LanguageRUS", "Head_Russian", "Head_Euro"};
        uniformClass = "T_CHDKZ_chedak4_uniform";
        model = "\rhsafrf\addons\rhs_infantry2\rhs_emr_base.p3d";
        selectionClan = "";
        class EventHandlers : EventHandlers {
            init = "";
        };
        headgearList[] = {};
        hiddenSelections[] = {"camo1", "camo2", "camob", "insignia", "clan"};
        hiddenSelectionsTextures[] = {
            "z\speclib\addons\units_chdkz_base\textures\soldier2_co.paa",
            "rhsafrf\addons\rhs_infantry\data\shevrons_vdv_co.paa",
            "rhsafrf\addons\rhs_infantry\data\digi_chevrons_co.paa",
            "",
            ""};
        class Wounds {
            tex[] = {};
            mat[] = {
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w1.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w2.rvmat",
                "A3\Characters_F\Common\Data\basicbody.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\characters_f\common\data\coveralls.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"};
        };
    };
    class T_CHDKZ_chedak5 : rhs_msv_emr_rifleman {
        scope = 1;
        scopeCurator = 0;
        identityTypes[] = {"LanguageRUS", "Head_Russian", "Head_Euro"};
        uniformClass = "T_CHDKZ_chedak5_uniform";
        model = "\rhsafrf\addons\rhs_infantry2\rhs_emr_base.p3d";
        selectionClan = "";
        class EventHandlers : EventHandlers {
            init = "";
        };
        headgearList[] = {};
        hiddenSelections[] = {"camo1", "camo2", "camob", "insignia", "clan"};
        hiddenSelectionsTextures[] = {
            "z\speclib\addons\units_chdkz_base\textures\soldier3_co.paa",
            "rhsafrf\addons\rhs_infantry\data\shevrons_vdv_co.paa",
            "rhsafrf\addons\rhs_infantry\data\digi_chevrons_co.paa",
            "",
            ""};
        class Wounds {
            tex[] = {};
            mat[] = {
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w1.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w2.rvmat",
                "A3\Characters_F\Common\Data\basicbody.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\characters_f\common\data\coveralls.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"};
        };
    };
    class T_CHDKZ_chedak6 : rhs_msv_emr_rifleman {
        scope = 1;
        scopeCurator = 0;
        identityTypes[] = {"LanguageRUS", "Head_Russian", "Head_Euro"};
        uniformClass = "T_CHDKZ_chedak6_uniform";
        model = "\rhsafrf\addons\rhs_infantry2\rhs_emr_base.p3d";
        selectionClan = "";
        class EventHandlers : EventHandlers {
            init = "";
        };
        headgearList[] = {};
        hiddenSelections[] = {"camo1", "camo2", "camob", "insignia", "clan"};
        hiddenSelectionsTextures[] = {
            "z\speclib\addons\units_chdkz_base\textures\soldier4_co.paa",
            "rhsafrf\addons\rhs_infantry\data\shevrons_vdv_co.paa",
            "rhsafrf\addons\rhs_infantry\data\digi_chevrons_co.paa",
            "",
            ""};
        class Wounds {
            tex[] = {};
            mat[] = {
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w1.rvmat",
                "rhsafrf\addons\rhs_infantry2\data\rhs_emr_w2.rvmat",
                "A3\Characters_F\Common\Data\basicbody.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\characters_f\common\data\coveralls.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Common\Data\coveralls_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"};
        };
    };
    class SpecLib_I_CHDKZ_chedak1 : T_CHDKZ_chedak1 {
        side = 2;
    };
    class SpecLib_I_CHDKZ_chedak2 : T_CHDKZ_chedak2 {
        side = 2;
    };
    class SpecLib_I_CHDKZ_chedak3 : T_CHDKZ_chedak3 {
        side = 2;
    };
    class SpecLib_I_CHDKZ_chedak4 : T_CHDKZ_chedak4 {
        side = 2;
    };
    class SpecLib_I_CHDKZ_chedak5 : T_CHDKZ_chedak5 {
        side = 2;
    };
    class SpecLib_I_CHDKZ_chedak6 : T_CHDKZ_chedak6 {
        side = 2;
    };
};
class UniformSlotInfo;
class UniformItem;
class CfgWeapons {
    class Itemcore;
    class rhs_uniform_flora;
    class T_CHDKZ_chedak1_uniform : rhs_uniform_flora {
        PUBLIC;
        displayName = "ChDKZ uniform 1";
        class ItemInfo : UniformItem {
            uniformClass = "T_CHDKZ_chedak1";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class T_CHDKZ_chedak2_uniform : rhs_uniform_flora {
        PUBLIC;
        displayName = "ChDKZ uniform 2";
        class ItemInfo : UniformItem {
            uniformClass = "T_CHDKZ_chedak2";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class T_CHDKZ_chedak3_uniform : rhs_uniform_flora {
        PUBLIC;
        displayName = "ChDKZ uniform 3";
        class ItemInfo : UniformItem {
            uniformClass = "T_CHDKZ_chedak3";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class T_CHDKZ_chedak4_uniform : rhs_uniform_flora {
        PUBLIC;
        displayName = "ChDKZ uniform 4";
        class ItemInfo : UniformItem {
            uniformClass = "T_CHDKZ_chedak4";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class T_CHDKZ_chedak5_uniform : rhs_uniform_flora {
        PUBLIC;
        displayName = "ChDKZ uniform 5";
        class ItemInfo : UniformItem {
            uniformClass = "T_CHDKZ_chedak5";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class T_CHDKZ_chedak6_uniform : rhs_uniform_flora {
        PUBLIC;
        displayName = "ChDKZ uniform 6";
        class ItemInfo : UniformItem {
            uniformClass = "T_CHDKZ_chedak6";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class SpecLib_I_CHDKZ_chedak1_uniform : T_CHDKZ_chedak1_uniform {
        PROTECTED;
        displayName = "ChDKZ uniform 1";
        class ItemInfo : UniformItem {
            uniformClass = "SpecLib_I_CHDKZ_chedak1";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class SpecLib_I_CHDKZ_chedak2_uniform : T_CHDKZ_chedak2_uniform {
        PROTECTED;
        displayName = "ChDKZ uniform 2";
        class ItemInfo : UniformItem {
            uniformClass = "SpecLib_I_CHDKZ_chedak2";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class SpecLib_I_CHDKZ_chedak3_uniform : T_CHDKZ_chedak3_uniform {
        PROTECTED;
        displayName = "ChDKZ uniform 3";
        class ItemInfo : UniformItem {
            uniformClass = "SpecLib_I_CHDKZ_chedak3";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class SpecLib_I_CHDKZ_chedak4_uniform : T_CHDKZ_chedak4_uniform {
        PROTECTED;
        displayName = "ChDKZ uniform 4";
        class ItemInfo : UniformItem {
            uniformClass = "SpecLib_I_CHDKZ_chedak4";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class SpecLib_I_CHDKZ_chedak5_uniform : T_CHDKZ_chedak5_uniform {
        PROTECTED;
        displayName = "ChDKZ uniform 5";
        class ItemInfo : UniformItem {
            uniformClass = "SpecLib_I_CHDKZ_chedak5";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class SpecLib_I_CHDKZ_chedak6_uniform : T_CHDKZ_chedak6_uniform {
        PROTECTED;
        displayName = "ChDKZ uniform 6";
        class ItemInfo : UniformItem {
            uniformClass = "SpecLib_I_CHDKZ_chedak6";
            containerClass = "Supply40";
            mass = 40;
        };
    };
    class HeadGearItem;
    class T_ChDKZ_Beret : Itemcore {
        scope = 2;
        displayName = "CHDKZ beret black";
        picture = "\z\speclib\addons\units_chdkz_base\textures\beret_ico.paa";
        model = "z\speclib\addons\units_chdkz_base\models\Beret.p3d";
        class ItemInfo : HeadGearItem {
            mass = 10;
            uniformmodel = "z\speclib\addons\units_chdkz_base\models\Beret.p3d";
            allowedSlots[] = {801, 901, 701, 605};
            modelSides[] = {6};
            armor = 0;
            passThrough = 1;
            class HitpointsProtectionInfo {
                class Head {
                    hitpointName = "HitHead";
                    armor = 0;
                    passThrough = 1;
                };
            };
        };
    };
};
