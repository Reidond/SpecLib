# speclib_nametags

Adds nametags above other players to simulate the familiarity with the faces with others one would have in real life.

## Maintainers

The people responsible for merging changes to this component or answering potential questions.

-   [Reidond](https://github.com/Reidond)
