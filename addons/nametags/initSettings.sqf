// CBA Settings [ADDON: speclib_nametags]:

[
    QGVAR(defaultNametagColor),
    "COLOR",
    [LSTRING(DefaultNametagColor)],
    ["SPEC","Nametags"],
    [0.77, 0.51, 0.08, 1],
    false, // isGlobal
    {[QGVAR(defaultNametagColor), _this] call ace_common_fnc_cbaSettings_settingChanged}
] call CBA_settings_fnc_init;

[
    QGVAR(nametagColorMain),
    "COLOR",
    ["str_team_main"],
    ["SPEC","Nametags"],
    [1.00, 1.00, 1.00, 1],
    false, // isGlobal
    {[QGVAR(nametagColorMain), _this] call ace_common_fnc_cbaSettings_settingChanged}
] call CBA_settings_fnc_init;

[
    QGVAR(nametagColorRed),
    "COLOR",
    ["str_team_red"],
    ["SPEC","Nametags"],
    [1.00, 0.67, 0.67, 1],
    false, // isGlobal
    {[QGVAR(nametagColorRed), _this] call ace_common_fnc_cbaSettings_settingChanged}
] call CBA_settings_fnc_init;

[
    QGVAR(nametagColorGreen),
    "COLOR",
    ["str_team_green"],
    ["SPEC","Nametags"],
    [0.67, 1.00, 0.67, 1],
    false, // isGlobal
    {[QGVAR(nametagColorGreen), _this] call ace_common_fnc_cbaSettings_settingChanged}
] call CBA_settings_fnc_init;

[
    QGVAR(nametagColorBlue),
    "COLOR",
    ["str_team_blue"],
    ["SPEC","Nametags"],
    [0.67, 0.67, 1.00, 1],
    false, // isGlobal
    {[QGVAR(nametagColorBlue), _this] call ace_common_fnc_cbaSettings_settingChanged}
] call CBA_settings_fnc_init;

[
    QGVAR(nametagColorYellow),
    "COLOR",
    ["str_team_yellow"],
    ["SPEC","Nametags"],
    [1.00, 1.00, 0.67, 1],
    false, // isGlobal
    {[QGVAR(nametagColorYellow), _this] call ace_common_fnc_cbaSettings_settingChanged}
] call CBA_settings_fnc_init;
