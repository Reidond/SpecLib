#include "script_component.hpp"

class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"ace_interaction"};
        author = "SpecLibTeam";
        authors[] = { "commy2", "esteldunedain", "Reidond" };
        VERSION_CONFIG;
    };
};

#include "CfgEventHandlers.hpp"
#include "ACE_Settings.hpp"
#include "CfgFactionClasses.hpp"
#include "CfgVehicles.hpp"

#include <RscTitles.hpp>
