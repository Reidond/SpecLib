class CfgVehicles {
    class Logic;
    class Module_F : Logic {
        class AttributesBase {
            class Default;
            class Edit;           // Default edit box (i.e., text input field)
            class Combo;          // Default combo box (i.e., drop-down menu)
            class Checkbox;       // Default checkbox (returned value is Bool)
            class CheckboxNumber; // Default checkbox (returned value is Number)
            class ModuleDescription; // Module description
            class Units; // Selection of units on which the module is applied
        };
        // Description base classes, for more information see below
        class ModuleDescription {
            class AnyBrain;
        };
    };
    class SpecLib_Module_EMP : Module_F {
        // Standard object definitions
        scope =
            2; // Editor visibility; 2 will show it in the menu, 1 will hide it.
        displayName = CSTRING(moduleName); // Name displayed in the menu
        // icon = "\myTag_addonName\data\iconNuke_ca.paa"; // Map icon. Delete
        // this entry to use the default icon
        category = "SpecLib_Modules";

        // Name of function triggered once conditions are met
        function = QFUNC(moduleEMP);
        // Execution priority, modules with lower number are executed first. 0
        // is used when the attribute is undefined
        functionPriority = 1;
        // 0 for server only execution, 1 for global execution, 2 for persistent
        // global execution
        isGlobal = 1;
        // 1 for module waiting until all synced triggers are activated
        isTriggerActivated = 0;
        // 1 if modules is to be disabled once it's activated (i.e., repeated
        // trigger activation won't work)
        isDisposable = 0;
        // // 1 to run init function in Eden Editor as well
        is3DEN = 0;

        // Menu displayed when the module is placed or double-clicked on by Zeus
        curatorInfoType = "RscDisplayAttributeModuleEMP";

        // Module attributes, uses
        // https://community.bistudio.com/wiki/Eden_Editor:_Configuring_Attributes#Entity_Specific
        class Attributes : AttributesBase {
            class Units : Units {
                property = "SOF_moduleEmp_Units";
            };
            // Module specific arguments
            class speclib_emp_objectName : Edit {
                // Unique property, use "<moduleClass>_<attributeClass>" format
                // to make sure the name is unique in the world
                property = "SOF_moduleEMP_objectName";
                displayName = CSTRING(objectName);    // Argument label
                tooltip = CSTRING(tooltipObjectName); // Tooltip description
                typeName =
                    "STRING"; // Value type, can be "NUMBER", "STRING" or "BOOL"
                defaultValue =
                    ""; // Default attribute value. WARNING: This is an
                              // expression, and its returned value will be used
                              // (50 in this case)
            };
            class speclib_emp_empRange : Edit {
                // Unique property, use "<moduleClass>_<attributeClass>" format
                // to make sure the name is unique in the world
                property = "SOF_moduleEMP_empRange";
                displayName = CSTRING(empRange);    // Argument label
                tooltip = CSTRING(tooltipEmpRange); // Tooltip description
                typeName =
                    "NUMBER"; // Value type, can be "NUMBER", "STRING" or "BOOL"
                defaultValue = "500"; // Default attribute value. WARNING: This
                                      // is an expression, and its returned
                                      // value will be used (50 in this case)
            };
            class speclib_emp_visibleEmp : Checkbox {
                // Unique property, use "<moduleClass>_<attributeClass>" format
                // to make sure the name is unique in the world
                property = "SOF_moduleEMP_visibleEmp";
                displayName = CSTRING(visibleEmp);    // Argument label
                tooltip = CSTRING(tooltipVisibleEmp); // Tooltip description
                typeName =
                    "BOOL"; // Value type, can be "NUMBER", "STRING" or "BOOL"
                defaultValue = "true"; // Default attribute value. WARNING: This
                                       // is an expression, and its returned
                                       // value will be used (50 in this case)
            };
            class speclib_emp_affectPerception : Checkbox {
                // Unique property, use "<moduleClass>_<attributeClass>" format
                // to make sure the name is unique in the world
                property = "SOF_moduleEMP_affectPerception";
                displayName = CSTRING(affectPerception); // Argument label
                tooltip =
                    CSTRING(tooltipAffectPerception); // Tooltip description
                typeName =
                    "BOOL"; // Value type, can be "NUMBER", "STRING" or "BOOL"
                defaultValue = "true"; // Default attribute value. WARNING: This
                                       // is an expression, and its returned
                                       // value will be used (50 in this case)
            };
            class speclib_emp_damageUnit : Edit {
                // Unique property, use "<moduleClass>_<attributeClass>" format
                // to make sure the name is unique in the world
                property = "SOF_moduleEMP_damageUnit";
                displayName = CSTRING(damageUnit);    // Argument label
                tooltip = CSTRING(tooltipDamageUnit); // Tooltip description
                typeName =
                    "NUMBER"; // Value type, can be "NUMBER", "STRING" or "BOOL"
                defaultValue = "0.1"; // Default attribute value. WARNING: This
                                      // is an expression, and its returned
                                      // value will be used (50 in this case)
            };
        };
    };
};
