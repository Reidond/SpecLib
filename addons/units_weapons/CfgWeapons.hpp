class CfgWeapons {
    class rhs_weap_rpg7;
    class CUP_hgun_PB6P9;
    class CUP_arifle_AKS74;
    class CUP_arifle_AKS74U;
    class CUP_arifle_AK74M_GL;
    class CUP_arifle_AK74M_GL_camo;
    class CUP_arifle_AK74M;
    class CUP_arifle_RPK74M;
    class gs_9a91;
    class gs_vsk94;
    class gs_vss_c;
    class VTN_SVD_1963;
    class VTN_SVD;
    class VTN_SVD_CAMO;
    class rhs_weap_l1a;

    class rhs_weap_rpg7_pgo7v3 : rhs_weap_rpg7 {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CowsSlot";
                item = "rhs_acc_pgo7v3";
            };
        };
    };

    class CUP_hgun_PB6P9_muzzle : CUP_hgun_PB6P9 {
        scope = 1;
        class LinkedItems {
            class LinkedItemsMuzzle {
                slot = "MuzzleSlot";
                item = "CUP_muzzle_PB6P9";
            };
        };
    };

    class SpecLib_arifle_AKS74_PSO1 : CUP_arifle_AKS74 {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "CUP_optic_PSO_1_AK";
            };
        };
    };

    class SpecLib_arifle_AKS74_GOSHAWK : CUP_arifle_AKS74 {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "CUP_optic_GOSHAWK";
            };
        };
    };

    class SpecLib_arifle_AKS74_NSPU : CUP_arifle_AKS74 {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "CUP_optic_NSPU";
            };
        };
    };

    class CUP_arifle_AKS74U_PBS4 : CUP_arifle_AKS74U {
        scope = 1;
        class LinkedItems {
            class LinkedItemsMuzzle {
                slot = "CUP_EastMuzzleSlotAK";
                item = "CUP_muzzle_PBS4";
            };
        };
    };

    class CUP_arifle_AK74M_GL_ekp1 : CUP_arifle_AK74M_GL {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "rhs_acc_ekp1";
            };
        };
    };

    class CUP_arifle_AK74M_ekp1 : CUP_arifle_AK74M {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "rhs_acc_ekp1";
            };
        };
    };

    class CUP_arifle_AK74M_GL_camo_tgpa : CUP_arifle_AK74M_GL_camo {
        scope = 1;
        class LinkedItems {
            class LinkedItemsMuzzle {
                slot = "CUP_EastMuzzleSlotAK";
                item = "CUP_muzzle_TGPA_woodland";
            };
        };
    };

    class CUP_arifle_AK74M_GL_gp25_tgpa_ekp1 : CUP_arifle_AK74M_GL {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "rhs_acc_ekp1";
            };

            class LinkedItemsMuzzle {
                slot = "CUP_EastMuzzleSlotAK";
                item = "rhs_acc_tgpa";
            };
        };
    };

    class CUP_arifle_AK74M_1p78 : CUP_arifle_AK74M {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "rhs_acc_1p78";
            };
        };
    };

    class CUP_arifle_AK74M_1p29 : CUP_arifle_AK74M {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "rhs_acc_1p29";
            };
        };
    };

    class CUP_arifle_AK74M_GL_1p78 : CUP_arifle_AK74M_GL {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "rhs_acc_1p78";
            };
        };
    };

    class CUP_arifle_AK74M_GL_1p29 : CUP_arifle_AK74M_GL {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "rhs_acc_1p29";
            };
        };
    };

    class CUP_arifle_AK74M_GL_1p63 : CUP_arifle_AK74M_GL {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "rhs_acc_1p63";
            };
        };
    };

    class SpecLib_arifle_RPK74M_1p78 : CUP_arifle_RPK74M {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CUP_DovetailMount_AK";
                item = "rhs_acc_1p78";
            };
        };
    };

    class gs_9a91_pbs_okp7 : gs_9a91 {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CowsSlot";
                item = "rhs_acc_okp7_dovetail";
            };

            class LinkedItemsMuzzle {
                slot = "MuzzleSlot";
                item = "gs_acc_vsk94_pbs";
            };
        };
    };

    class gs_vsk94_pbs_pso1_1 : gs_vsk94 {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CowsSlot";
                item = "gs_acc_pso1_1";
            };

            class LinkedItemsMuzzle {
                slot = "MuzzleSlot";
                item = "gs_acc_vsk94_pbs";
            };
        };
    };

    class gs_vss_camo_pso : gs_vss_c {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CowsSlot";
                item = "gs_acc_pso1_1_vss";
            };
            class LinkedItemsMuzzle {
                slot = "MuzzleSlot";
                item = "gs_acc_separator9x39vss";
            };
        };
    };

    class VTN_SVD_1963_PSO1 : VTN_SVD_1963 {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CowsSlot";
                item = "CUP_optic_PSO_1";
            };
        };
    };

    class VTN_SVD_1963_NSPU : VTN_SVD_1963 {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CowsSlot";
                item = "CUP_optic_NSPU";
            };
        };
    };

    class VTN_SVD_CAMO_PART_PSO1M2 : VTN_SVD_CAMO {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CowsSlot";
                item = "CUP_optic_PSO_1";
            };
            class LinkedItemsAcc {
                slot = "PointerSlot";
                item = "VTN_CAMO_GRASS_PART_SVD";
            };
        };
    };

    class VTN_SVD_PSO1M2 : VTN_SVD {
        scope = 1;
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CowsSlot";
                item = "CUP_optic_PSO_1";
            };
        };
    };
};
