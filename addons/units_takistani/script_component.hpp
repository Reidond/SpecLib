#define COMPONENT units_takistani
#define COMPONENT_BEAUTIFIED Units - Takistani
#include "\z\speclib\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_TEST
#define DEBUG_MODE_FULL
#endif
#ifdef DEBUG_SETTINGS_TEST
#define DEBUG_SETTINGS DEBUG_SETTINGS_TEST
#endif

#include "\z\speclib\addons\main\script_macros.hpp"

#define DEFAULT_CIV_EQUIPMENT                                                  \
    weapons[] = {"Throw", "Put"};                                              \
    respawnWeapons[] = {"Throw", "Put"};                                       \
    magazines[] = {};                                                          \
    respawnMagazines[] = {};                                                   \
    Items[] = {};                                                              \
    RespawnItems[] = {}

#define DEFAULT_CIV_UNIT_ENTRIES                                               \
    author = CSTRING(SpecLibTeam);                                             \
    scope = 2;                                                                 \
    dlc = "speclib";                                                           \
    scopeCurator = 2;                                                          \
    accuracy = 3.9;                                                            \
    selectionClan = "";                                                        \
    class EventHandlers {                                                      \
        class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers {};      \
    };                                                                         \
    headgearList[] = {}

#define DEFAULT_GUE_UNIT_ENTRIES                                               \
    author = CSTRING(SpecLibTeam);                                             \
    scope = 2;                                                                 \
    dlc = "speclib";                                                           \
    scopeCurator = 2;                                                          \
    selectionClan = "";                                                        \
    class EventHandlers {                                                      \
        class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers {};      \
    };                                                                         \
    headgearList[] = {}

#define DEFAULT_BOOMER_ITEMS "ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
#define DEFAULT_ACE_ITEMS                                                \
    "ACE_morphine", "ACE_tourniquet", "ACE_fieldDressing",                     \
        "ACE_fieldDressing", "ACE_packingBandage", "ACE_packingBandage",       \
        "ACE_elasticBandage", "ACE_elasticBandage", "ACE_quikclot",            \
        "ACE_quikclot", "ACE_EarPlugs"
