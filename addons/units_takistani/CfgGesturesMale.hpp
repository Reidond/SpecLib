class CfgGesturesMale {
    class default;
    class States {
        class GestureReloadMK1 : default {
            file =
                "\z\speclib\addons\units_takistani\weapons\bnae_mk1\Anim\MK1reload.rtm";
            looped = 0;
            speed = -5;
            mask = "handsWeapon";
            headBobStrength = 0.2;
            headBobMode = 2;
            canPullTrigger = 0;
            rightHandIKCurve[] = {0,    1, 0.02, 1, 0.04, 0,
                                  0.96, 0, 0.98, 1, 1,    1};
            leftHandIKBeg = 1;
            leftHandIKEnd = 1;
            leftHandIKCurve[] = {1};
            preload = 1;
        };
        class GestureReloadMK1t : GestureReloadMK1 {
            file =
                "\z\speclib\addons\units_takistani\weapons\bnae_mk1\Anim\MK1treload.rtm";
            rightHandIKCurve[] = {0,    1, 0.62,     1, 0.65, 0,
                                  0.95, 0, 0.996667, 1, 1,    1};
            leftHandIKCurve[] = {0,        1, 0.01, 1, 0.036667, 0,
                                 0.586667, 0, 0.62, 1, 1,        1};
            speed = -4.2;
        };
        class GestureReloadMK1Context : GestureReloadMK1 {
            mask = "handsWeapon_context";
        };
        class GestureReloadMK1tContext : GestureReloadMK1t {
            mask = "handsWeapon_context";
        };
        class GestureReloadMK1Prone : GestureReloadMK1 {
            file =
                "\z\speclib\addons\units_takistani\weapons\bnae_mk1\Anim\MK1reloadprone.rtm";
            headBobStrength = 0.1;
        };
        class GestureReloadMK1tProne : GestureReloadMK1t {
            file =
                "\z\speclib\addons\units_takistani\weapons\bnae_mk1\Anim\MK1treloadprone.rtm";
            headBobStrength = 0.1;
        };
        class GestureFiredMK1 : default {
            file =
                "\z\speclib\addons\units_takistani\weapons\bnae_mk1\Anim\MK1Fired.rtm";
            enableOptics = 0;
            looped = 0;
            speed = -1;
            mask = "handsWeapon";
            headBobStrength = 0.2;
            headBobMode = 2;
            canPullTrigger = 0;
            rightHandIKCurve[] = {0, 1, 0.05, 1, 0.1, 0, 0.9, 0, 0.96, 1, 1, 1};
            leftHandIKBeg = 1;
            leftHandIKEnd = 1;
            leftHandIKCurve[] = {1};
            preload = 1;
        };
        class GestureFiredMK1Context : GestureFiredMK1 {
            mask = "handsWeapon_context";
        };
        class GestureFiredMK1Prone : GestureFiredMK1 {
            file =
                "\z\speclib\addons\units_takistani\weapons\bnae_mk1\Anim\MK1firedprone.rtm";
            headBobStrength = 0.1;
        };
    };
};
