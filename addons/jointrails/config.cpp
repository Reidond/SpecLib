#include "script_component.hpp"
class CfgPatches {
    class ADDON {
        name = COMPONENT;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {
            "cba_main",
            "ace_main",
            "CUP_Weapons_WeaponsCore",
            "CUP_Weapons_Sounds",
            "CUP_Weapons_East_Attachments",
            "CUP_Weapons_Ammunition",
            "rhs_c_weapons",
            "asdg_jointrails",
            "GS_A3_Weapons"
        };
        author = "Reidond";
        VERSION_CONFIG;
    };
};

#include "CfgEventHandlers.hpp"

class asdg_OpticRail {
    class compatibleItems;
};
class asdg_OpticSideMount: asdg_OpticRail {
    class compatibleItems: compatibleItems {};
};
class asdg_OpticSideRail_AKSVD: asdg_OpticSideMount {
    class compatibleItems: compatibleItems {};
};
class rhs_russian_rifle_scopes_slot: asdg_OpticSideRail_AKSVD {
    class compatibleItems: compatibleItems {};
};
class CUP_PicatinnySideMount: rhs_russian_rifle_scopes_slot {
    class compatibleItems: compatibleItems {};
};
class gs_vss_base_scopes_slot: rhs_russian_rifle_scopes_slot {
    class compatibleItems: compatibleItems {};
};
