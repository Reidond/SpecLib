class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"A3_Data_F_Loadorder", "cba_xeh", "cba_main"};
        author = "Reidond";
        authors[] = {""};
        url = "https://gitlab.com/Reidond";
        VERSION_CONFIG;
    };
};
