class CfgWeapons {
    class Binocular;
    class UK3CB_BAF_Soflam_Laserdesignator : Binocular {
        scope = 2;
        author = "www.3commandobrigade.com";
        DLC = "speclib";
        displayName = "AN/PEQ-1 SOFLAM";
        _generalMacro = "Laserdesignator";
        model = "\z\speclib\addons\soflam\models\soflam.p3d";
        modelOptics = "\z\speclib\addons\soflam\models\optika_SOFLAM";
        opticsPPEffects[] = {"OpticsCHAbera1", "OpticsBlur1"};
        descriptionUse = "";
        picture = "\z\speclib\addons\soflam\data\ui\gear_soflam_ca.paa";
        magazines[] = {"Laserbatteries"};
        cursor = "laserDesignator";
        cursorAim = "EmptyCursor";
        cursorAimOn = "EmptyCursor";
        showSwitchAction = 1;
        simulation = "weapon";
        forceOptics = 0;
        side = 1;
        class WeaponSlotsInfo {
            mass = 55;
        };
        laser = 1;
        descriptionShort = "$STR_DSS_Laserdesignator";
        weaponInfoType = "RscOptics_LaserDesignator";
        visionMode[] = {"Normal", "NVG", "TI"};
        thermalMode[] = {2};
        opticsZoomMin = 0.0125;
        opticsZoomMax = 0.1242;
        opticsZoomInit = 0.1242;
        distanceZoomMin = 100;
        distanceZoomMax = 2300;
    };
};
